(function() {
	$('#form-text').on('submit', function(event){
		event.preventDefault();
		var text = $('#text').val();
		var braille = $('#braille');

		$.ajax({
			url: '/convert',
			type: 'POST',
			data: {value : text}
		})
		.done(function(data) {
			console.log(data);
			braille.val(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		

		var brailleText = convertToBraille(text);
	});
})();